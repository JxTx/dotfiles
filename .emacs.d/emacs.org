* General settings
  First, let’s increase the cache before starting garbage collection:
  #+BEGIN_SRC emacs-lisp
    (setq gc-cons-threshold 50000000)
  #+END_SRC
  remove the warnings from the GnuTLS library when using HTTPS… increase the minimum prime bits size:
  #+BEGIN_SRC emacs-lisp
    (setq gnutls-min-prime-bits 4096)
  #+END_SRC
* Package Initialization
  Lets start with the package repos
  #+BEGIN_SRC emacs-lisp
    (require 'package)

    (setq package-archives '(("gnu"       . "http://elpa.gnu.org/packages/")
			     ("melpa"     . "https://melpa.org/packages/")))

    ;;(package-initialize)
  #+END_SRC

  #+begin_src emacs-lisp
    (setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")
  #+end_src
* Use-Package
  #+BEGIN_SRC emacs-lisp
    (unless (package-installed-p 'use-package)
      (package-refresh-contents)
      (package-install 'use-package))

    (require 'use-package)
  #+END_SRC
* Diminish
This removes the minor modes in the status bar. It keeps it tidy
  #+BEGIN_SRC emacs-lisp
    (use-package diminish
      :ensure t)
  #+END_SRC
* UI tweaks
** Full screen
   Why would you not like emacs in full-screen ?
  #+BEGIN_SRC emacs-lisp
    (toggle-frame-maximized)
  #+END_SRC
** Save State
  #+BEGIN_SRC emacs-lisp
    (desktop-save-mode 1)
  #+END_SRC
** Always use UTF-8

   #+BEGIN_SRC emacs-lisp
     (set-charset-priority 'unicode)
     (set-default-coding-systems 'utf-8)
     (set-terminal-coding-system 'utf-8)
     (set-keyboard-coding-system 'utf-8)
     (set-selection-coding-system 'utf-8)
     (prefer-coding-system 'utf-8)
   #+END_SRC
** Remove the alarm bell
   The bell is the most annoying thing in emacs
   #+BEGIN_SRC emacs-lisp
     (setq ring-bell-function 'ignore)
   #+END_SRC
** Remove menu bar
   The more screen space the better
   #+BEGIN_SRC emacs-lisp
     (menu-bar-mode -1)
     (if (display-graphic-p)
	 (progn
	   (tool-bar-mode -1)
	   (scroll-bar-mode -1)))
   #+END_SRC
** Wrap text
   #+BEGIN_SRC emacs-lisp
     (global-visual-line-mode t)
   #+END_SRC
** Right Click Copy
   #+BEGIN_SRC emacs-lisp
     (defadvice mouse-save-then-kill (around mouse2-copy-region activate)
       (when (region-active-p)
	 (copy-region-as-kill (region-beginning) (region-end)))
       ad-do-it)
   #+END_SRC
** Syntax highlighting
   #+BEGIN_SRC emacs-lisp
     (global-font-lock-mode 1)
   #+END_SRC
** Window Margins
#+begin_src emacs-lisp
  (set-window-margins nil 15)
#+end_src
** Column number
   #+BEGIN_SRC emacs-lisp
     (setq column-number-mode t)
   #+END_SRC
** y/n over yes/no
   Why would you want to type in *yes* over *y* ?
   #+BEGIN_SRC emacs-lisp
     (fset 'yes-or-no-p 'y-or-n-p)
   #+END_SRC
** Indent to 2 rather than 4
   #+BEGIN_SRC emacs-lisp
     (setq standard-indent 2)
   #+END_SRC
** Python Indent
Python indented 8 once, lets make sure its not trying to guess the indents for me.

#+begin_src emacs-lisp
  (setq python-indent-guess-indent-offset nil)
#+end_src
** Auto update buffers
   #+BEGIN_SRC emacs-lisp
     (global-auto-revert-mode t)
   #+END_SRC
** Resolve symlinks
   #+BEGIN_SRC emacs-lisp
     (setq-default find-file-visit-truename t)
   #+END_SRC
** Stop emacs temp files
   #+BEGIN_SRC emacs-lisp
     (setq create-lockfiles nil)
   #+END_SRC
** Stop creating ~ files and move # file to /tmp
   Any ~ file and # file move to /tmp
   #+BEGIN_SRC emacs-lisp
     (setq make-backup-files nil)
     (setq backup-directory-alist
	   `((".*" . ,temporary-file-directory)))
     (setq auto-save-file-name-transforms
	   `((".*" ,temporary-file-directory t)))
   #+END_SRC
** Hide the startup message
   Remove that splash screen
   #+BEGIN_SRC emacs-lisp
     (setq inhibit-startup-message t) 
   #+END_SRC
** Remove the messages buffer
   #+BEGIN_SRC emacs-lisp
     (setq-default message-log-max nil)
     (kill-buffer "*Messages*")
   #+END_SRC
** Remove Completions from buffer
   #+BEGIN_SRC emacs-lisp
     (add-hook 'minibuffer-exit-hook
	   '(lambda ()
	      (let ((buffer "*Completions*"))
		(and (get-buffer buffer)
		     (kill-buffer buffer)))))
   #+END_SRC
** Make scratch empty
   We know what the scratch is, so i don't neet to be told every time
   #+BEGIN_SRC emacs-lisp
     (setq initial-scratch-message "")
   #+END_SRC
** Make scratch Org'able
   #+BEGIN_SRC emacs-lisp
     (setq initial-major-mode 'org-mode)
   #+END_SRC
** Highlight current line 
   #+BEGIN_SRC emacs-lisp
     (global-hl-line-mode 1)
   #+END_SRC
** Hide the mouse while typing
   #+BEGIN_SRC emacs-lisp
     (setq make-pointer-invisible t)
   #+END_SRC
** Delete selected region automatically when typing
   #+BEGIN_SRC emacs-lisp
     (delete-selection-mode 1)
   #+END_SRC
** Kill Current Buffer
   #+BEGIN_SRC emacs-lisp
     (defun kill-current-buffer ()
       (interactive)
       (kill-buffer (current-buffer)))
     (global-set-key (kbd "C-x k") 'kill-current-buffer)
   #+END_SRC
** Define ibuffer binding
#+begin_src emacs-lisp
  (global-set-key (kbd "C-x C-b") 'ibuffer-list-buffers)
#+end_src
** Following window splits
   #+BEGIN_SRC emacs-lisp
     (defun split-and-follow-horizontally ()
       (interactive)
       (split-window-below)
       (balance-windows)
       (other-window 1))
     (global-set-key (kbd "C-x 2") 'split-and-follow-horizontally)

     (defun split-and-follow-vertically ()
       (interactive)
       (split-window-right)
       (balance-windows)
       (other-window 1))
     (global-set-key (kbd "C-x 3") 'split-and-follow-vertically)
   #+END_SRC
** Dired Tweaks
   #+begin_src emacs-lisp
     (use-package dired-subtree
       :ensure t
       :config
       (bind-key "<tab>" #'dired-subtree-toggle dired-mode-map)
       (bind-key "<backtab>" #'dired-subtree-cycle dired-mode-map))
   #+end_src
   
show size in human readable format
#+begin_src emacs-lisp
  (setq dired-listing-switches "-alh")
#+end_src
*** all-the-icons-dired
#+begin_src emacs-lisp
  (use-package all-the-icons-dired
    :config
    (add-hook 'dired-mode-hook #'all-the-icons-dired-mode)
    (setq all-the-icons-dired-monochrome nil))
#+end_src
** Mac Hash
Allow hash to be entered
#+begin_src emacs-lisp
  (global-set-key (kbd "M-3") '(lambda () (interactive) (insert "#")))
#+end_src
* Theme
** Set frame border
#+begin_src emacs-lisp
  (add-to-list 'default-frame-alist '(internal-border-width . 7))
#+end_src
** Remove visual line mode indication
   #+BEGIN_SRC emacs-lisp
     (diminish 'visual-line-mode)
   #+END_SRC
** Time
   #+BEGIN_SRC emacs-lisp
     (setq display-time-day-and-date t)
     (display-time)
   #+END_SRC
** Doom modeline
   #+begin_src emacs-lisp
     (use-package doom-modeline
       :ensure t
       :hook
       (after-init . doom-modeline-init)
       :custom
       (doom-modeline-major-mode-icon t)
       (doom-modeline-buffer-file-name-style 'buffer-name)
       (doom-modeline-icon t)
       (doom-modeline-buffer-encoding nil)
       )
   #+end_src
** Smartparents
   #+BEGIN_SRC emacs-lisp
     (use-package smartparens
     :ensure t
     :diminish smartparens-mode
     :config
     (smartparens-global-mode)
     )
   #+END_SRC
** Theme
Nord is the way forward
   #+BEGIN_SRC emacs-lisp
     (use-package nord-theme
       :ensure t
       :config (load-theme 'nord t))
   #+END_SRC
** all-the-icons-ibuffer
#+begin_src emacs-lisp
(use-package all-the-icons-ibuffer
  :ensure t
  :init (all-the-icons-ibuffer-mode 1))
#+end_src
** Change cursor type
   #+begin_src emacs-lisp
     (setq-default cursor-type 'bar) 
   #+end_src
** Selective-display
   #+BEGIN_SRC emacs-lisp
     (defun toggle-selective-display (column)
       (interactive "P")
       (set-selective-display
	(or column
	    (unless selective-display
	      (1+ (current-column))))))

     (defun toggle-hiding (column)
       (interactive "P")
       (if hs-minor-mode
	   (if (condition-case nil
		   (hs-toggle-hiding)
		 (error t))
	       (hs-show-all))
	 (toggle-selective-display column)))
     (load-library "hideshow")

     (global-set-key (kbd "C-+") 'toggle-hiding)
     (global-set-key (kbd "C-\\") 'toggle-selective-display)
   #+END_SRC
* Shell stuff
** Tramp
9 times out of 10 we are going to have =bash= on the remote host, so lets make sure that tramp uses that over =zsh=
   #+BEGIN_SRC emacs-lisp
     (setq tramp-default-method "ssh")
     (setq explicit-shell-file-name "/bin/bash")
     (setq shell-file-name "/bin/bash")
   #+END_SRC
** Server
   #+BEGIN_SRC emacs-lisp
     (server-start)
     (setq server-socket-dir "~/.emacs.d/server")
   #+END_SRC
   now alias ~em~ to ~emacsclient -s ~/.emacs.d/server/server~ in your ~.zshrc~ file 
** VTerm
#+begin_src emacs-lisp
  (use-package vterm
    :ensure t
    :config
    (setq vterm-shell "zsh")
    ;;(setq vterm-kill-buffer-on-exit t)
    (add-to-list 'display-buffer-alist
       '("\*vterm\*"
         (display-buffer-in-side-window)
         (window-height . 0.3)
         (side . bottom)
         (slot . 0)))
    :init
    (add-hook 'vterm-exit-functions
       (lambda (_ _)
         (let* ((buffer (current-buffer))
                (window (get-buffer-window buffer)))
           (when (not (one-window-p))
             (delete-window window))
           (kill-buffer buffer))))
    :bind ("C-c t" . 'vterm-other-window)
    )
#+end_src
** Eshell
   #+BEGIN_SRC emacs-lisp
     (use-package eshell
       :init
       (add-hook 'eshell-mode-hook
		 (lambda ()
		   (add-to-list 'eshell-visual-commands "ssh")
		   (add-to-list 'eshell-visual-commands "tail")
		   (add-to-list 'eshell-visual-commands "top")
		   (add-to-list 'eshell-visual-commands "tmux"))
		 )
       )

     (defun eshell/clear ()
       "Clear the eshell buffer."
       (let ((inhibit-read-only t))
	 (erase-buffer)
	 (eshell-send-input)))

     (defun eshell-here ()
       (interactive)
	 (eshell "new"))

     (bind-key "C-!" 'eshell-here)
   #+END_SRC
* Packages
Yes, I do put the closing =)= on its own line, and I know thats not "the done thing". But it helps me debug. Too many times have I installed a package and come to reload emacs and I get an error about a miss-matched =)=.
** exec-path-from-shell
#+begin_src emacs-lisp
  (use-package exec-path-from-shell
    :ensure t
    :init (exec-path-from-shell-initialize)
  )  
#+end_src
** Multiple cursors
   #+BEGIN_SRC emacs-lisp
     (use-package multiple-cursors
     :ensure t
     :bind (("C-c d" . mc/edit-lines)
     ("C->" . mc/mark-next-like-this)
     ("C-<" . mc/mark-previous-like-this)
     ("C-c C-<" . mc/mark-all-like-this))
     )
   #+END_SRC
** Ivy mode
   #+BEGIN_SRC emacs-lisp
     (use-package ivy
     :ensure t
     :diminish ivy-mode
     )
     
     (use-package counsel
     :ensure t)
     
     (use-package swiper    
       :ensure t    
       :bind    
       (("C-s" . swiper)    
        ("M-x" . counsel-M-x)    
        ("C-x C-f" . counsel-find-file)    
        ("C-x l" . counsel-locate)
        ("C-x c" . counsel-flycheck)
        ("C-x C-r" . counsel-recentf)
        ("C-c C-r" . ivy-resume)
        ("C-r" . counsel-expression-history))
       :config    
       (ivy-mode 1)
       (setq ivy-count-format "(%d/%d) ")
     )
     
     (use-package ivy-rich
     :ensure t
     :config
     (ivy-rich-mode 1)
     )
   #+END_SRC

*** ivy all the icons
#+begin_src emacs-lisp
  (use-package all-the-icons-ivy-rich
    :ensure t
    :init (all-the-icons-ivy-rich-mode 1))
  
  (use-package ivy-rich
    :ensure t
    :init (ivy-rich-mode 1))
#+end_src
** cheat-sh
   #+BEGIN_SRC emacs-lisp
     (use-package cheat-sh
       :ensure t)
   #+END_SRC
** which-key
   #+BEGIN_SRC emacs-lisp
     (use-package which-key
       :ensure t
       :config
       (which-key-mode)
       :diminish 'which-key-mode
     )  
   #+END_SRC
** popup-kill-ring
   #+BEGIN_SRC emacs-lisp
          (use-package popup-kill-ring
            :ensure t
            :bind ("M-y" . popup-kill-ring)
            )
   #+END_SRC
** lsp-mode
#+begin_src emacs-lisp
  (use-package lsp-mode
    :ensure t
    :init
    (setq lsp-keymap-prefix "C-c l")
    :hook ((
           objc-mode
           cc-mode
           c-mode
           c++-mode
           json-mode
           dockerfile-mode
           sql-mode
           yamal-mode
           sh-mode
           ;;ps-mode
           python-mode
           go-mode
           swift-mode
           js-mode
           web-mode
           js-jsx-mode
           typescript-mode
           ) . lsp-deferred)
           ;; if you want which-key integration
           (lsp-mode . lsp-enable-which-key-integration)
    :commands lsp
    :config
    (setq lsp-auto-guess-root t)
    (setq lsp-log-io nil)
    (setq lsp-restart 'auto-restart)
    (setq lsp-enable-symbol-highlighting nil)
    (setq lsp-enable-on-type-formatting nil)
    (setq lsp-signature-auto-activate nil)
    (setq lsp-signature-render-documentation nil)
    (setq lsp-diagnostics-provider :flycheck)
    (setq lsp-eldoc-hook nil)
    (setq lsp-modeline-code-actions-enable nil)
    (setq lsp-modeline-diagnostics-enable nil)
    (setq lsp-headerline-breadcrumb-enable nil)
    (setq lsp-semantic-tokens-enable nil)
    (setq lsp-enable-folding nil)
    (setq lsp-enable-imenu nil)
    (setq lsp-enable-snippet nil)
    (setq read-process-output-max (* 1024 1024)) ;; 1MB
    (setq lsp-idle-delay 0.5)
    (setq lsp-enable-links nil)
    :custom
    (lsp-enable-snippet t)
    )
#+end_src

#+begin_src emacs-lisp
  (use-package lsp-ui
    :ensure t
    :commands lsp-ui-mode
    :config
    (setq lsp-ui-doc-enable nil)
    (setq lsp-ui-doc-header t)
    (setq lsp-ui-doc-include-signature t)
    (setq lsp-ui-doc-border (face-foreground 'default))
    (setq lsp-ui-sideline-show-code-actions t)
    (setq lsp-ui-sideline-delay 0.05)
    )
#+end_src

#+begin_src emacs-lisp
  (use-package lsp-ivy
    :ensure t
    :commands lsp-ivy-workspace-symbol)
#+end_src

*** Specific install requirements
**** Python
#+begin_src shell :results silent
  pip3 install pyright
#+end_src

#+begin_src emacs-lisp
  (use-package lsp-pyright
    :ensure t
    :init (when (executable-find "python3")
            (setq lsp-pyright-python-executable-cmd "python3"))
    :hook (python-mode . (lambda ()
                           (require 'lsp-pyright)
                           (lsp-deferred)))
    )
#+end_src

**** Golang
#+begin_src shell :results silent
  go install golang.org/x/tools/gopls@latest
#+end_src
**** SQL
#+begin_src shell :results silent
  go get github.com/lighttiger2505/sqls
#+end_src
**** Swift
#+begin_src emacs-lisp
  (use-package lsp-sourcekit
    :ensure t
    :after lsp-mode
    :config
    (setq lsp-sourcekit-executable (string-trim (shell-command-to-string "xcrun --find sourcekit-lsp")))
    )
#+end_src
** company-mode

#+begin_src emacs-lisp
  (use-package company
    :ensure t
    :hook (prog-mode . company-mode)
    :config
    (setq company-minimum-prefix-length 1)
    (setq company-selection-wrap-around t)
    (setq company-tooltip-align-annotations t)
    (setq company-frontends '(company-pseudo-tooltip-frontend ; show tooltip even for single candidate
                              company-echo-metadata-frontend))
    )
#+end_src

** Yasnippet
   #+BEGIN_SRC emacs-lisp
     (use-package yasnippet-snippets
       :ensure t)

     (use-package yasnippet
       :ensure t
       :init
       (yas-global-mode t)
       :config
       (setq yas-snippet-dirs '("~/.emacs.d/elpa/yasnippet-snippets-20210808.1851/snippets"))
       (yas-reload-all)
       :diminish 'yas-minor-mode
     )
   #+END_SRC
** Flycheck
   needed for bash scripts otherwise your'll go mad
   #+begin_example
   # shellcheck disable=SC2039
   # shellcheck disable=SC1117   
   #+end_example

   #+BEGIN_SRC emacs-lisp
     (use-package flycheck
       :ensure t
       :init
       (global-flycheck-mode t)
       :diminish 'flycheck-mode
     )  
   #+END_SRC

Fixes for Doom mode Line 
   #+begin_src emacs-lisp
     (setq doom-modeline-python-executable "python3")
     (setq python-shell-interpreter "python3")
     (setq flycheck-python-pycompile-executable "python3"
	   flycheck-python-pylint-executable "python3"
	   flycheck-python-flake8-executable "python3")
     (setq doom-modeline-major-mode-icon nil
	   doom-modeline-version t)
   #+end_src
** Flyspell
   #+BEGIN_SRC emacs-lisp
        (use-package flyspell
          :ensure t
          :defer 1
          :hook ((markdown-mode . flyspell-mode)
                 (org-mode . flyspell-mode))
          :custom
          (flyspell-issue-message-flag nil)
          (flyspell-mode 1)
     )
     
        (use-package flyspell-correct-ivy
          :ensure t
          :after flyspell
          :bind (:map flyspell-mode-map
                      ("C-;" . flyspell-correct-at-point))
          ;;("C-;" . flyspell-correct-word-generic))
          :custom (flyspell-correct-interface 'flyspell-correct-ivy))
        (eval-after-load "flyspell"
          '(diminish 'flyspell-mode))
   #+END_SRC
** Pipenv
   #+BEGIN_SRC emacs-lisp
     (use-package pipenv
       :hook (python-mode . pipenv-mode)
       :init
       (setq
        pipenv-projectile-after-switch-function
        #'pipenv-projectile-after-switch-extended))
   #+END_SRC
** Poetry
#+begin_src emacs-lisp
  (use-package poetry
    :ensure t)
#+end_src
** Bash Completion
   #+begin_src emacs-lisp
     (use-package bash-completion
       :ensure t
       :config (bash-completion-setup)
     )
   #+end_src
** dockerfile-mode
   #+BEGIN_SRC emacs-lisp
     (use-package dockerfile-mode
     :ensure t
     :init (add-to-list 'auto-mode-alist '("Dockerfile\\'" . dockerfile-mode))
     )
   #+END_SRC
** docker-mode
   #+BEGIN_SRC emacs-lisp
;;     (use-package docker
;;       :ensure t
;;     :diminish 'docker-mode
;;     )
   #+END_SRC
** web-mode
   #+BEGIN_SRC emacs-lisp
     (use-package web-mode
       :ensure t
       :mode ("\\.html\\'"
              "\\.css\\'")
       :config
       (progn
         (setq web-mode-code-indent-offset 2)
         (setq web-mode-enable-auto-quoting nil))
     )
   #+END_SRC
** web-beautify
We do need to install this as a dependency first
#+begin_src shell
  npm -g install js-beautify
#+end_src

#+begin_src emacs-lisp
  (use-package web-beautify
      :ensure t
      :bind (:map web-mode-map
             ("C-c b" . web-beautify-html)
             :map js2-mode-map
             ("C-c b" . web-beautify-js))
  )
#+end_src
** php-mode
   #+begin_src emacs-lisp
     (use-package php-mode
       :ensure t
       :mode ("\\.php\\'")
       )
   #+end_src
** js2-mode
   #+BEGIN_SRC emacs-lisp
     (use-package js2-mode
       :ensure t
       :mode ("\\.js\\'" . js2-mode)
       )
   #+END_SRC
** go-mode
   #+begin_src emacs-lisp
     (use-package go-mode
       :ensure t
       :mode (("\\.go\\'" . go-mode))
       )
   #+end_src

*gopath*

#+begin_src emacs-lisp
  (add-to-list 'exec-path (expand-file-name "~/go/bin"))
#+end_src

** powershell-mode
   #+begin_src emacs-lisp
     (use-package powershell
       :ensure t)
   #+end_src

** swift-mode
#+begin_src emacs-lisp
  (use-package swift-mode
    :ensure t
    :hook (swift-mode . (lambda () (lsp)))
    )
#+end_src
*** Flycheck Swift
#+begin_src emacs-lisp
    (use-package flycheck-swift
      :ensure t)
  (eval-after-load 'flycheck '(flycheck-swift-setup))
#+end_src
** json-mode
   #+begin_src emacs-lisp
     (use-package json-mode
       :ensure t
       :mode (("\\.json\\'" . json-mode))
     )
   #+end_src
** markdown-mode
   #+BEGIN_SRC emacs-lisp
     (use-package markdown-mode
       :mode "\\.md\\'"
       :ensure t)
   #+END_SRC
** yaml-mode
   #+BEGIN_SRC emacs-lisp
     (use-package yaml-mode
       :ensure t
       :config (require 'yaml-mode)
     (add-to-list 'auto-mode-alist '("\\.yml$" . yaml-mode))
     (add-hook 'yaml-mode-hook
               '(lambda () (define-key yaml-mode-map "\C-m" 'newline-and-indent)))
     )
   #+END_SRC
** nodejs-repl
#+begin_src emacs-lisp
  (use-package nodejs-repl
    :ensure t)
#+end_src
** Ag + ripgrep + grep
   #+begin_src emacs-lisp
     (use-package ag
       :ensure t)
   #+end_src

#+begin_src emacs-lisp
  (use-package ripgrep
    :ensure t)
#+end_src
   
   and fix some =grep= defaults
   #+begin_src emacs-lisp
     (setq grep-command "grep --color -n -rsE ")
     (setq grep-find-template
	"find <D> <X> -type f <F> -exec grep <C> -n -r -s -E <R> /dev/null \\{\\} +")
   #+end_src
   
   #+begin_src emacs-lisp
     (use-package visual-regexp
       :ensure t
       :bind ("C-c r" . vr/replace)
       ("C-c q" . vr/query-replace)
       ("C-c m" . vr/mc-mark))
   #+end_src
** magit
   #+BEGIN_SRC emacs-lisp
     (use-package magit
       :ensure t
       :commands magit-status
       :config
       (progn
         (magit-auto-revert-mode 1)
         (setq magit-completing-read-function 'ivy-completing-read))
       :init
       (add-hook 'magit-mode-hook 'magit-load-config-extensions)
       :bind
       ("M-g" . magit-status)
     )  

     (use-package magithub
       :after magit
       :ensure t
       :disabled
       :config (magithub-feature-autoinject t)
     )  
   #+END_SRC
*** Git Gutter
#+begin_src emacs-lisp
  (use-package git-gutter
    :ensure t
    :config
    (setq git-gutter:update-interval 0.05)
    (global-git-gutter-mode +1)
  )  
#+end_src

#+begin_src emacs-lisp
  (use-package git-gutter-fringe
    :ensure t
    :config
    (define-fringe-bitmap 'git-gutter-fr:added [224] nil nil '(center repeated))
    (define-fringe-bitmap 'git-gutter-fr:modified [224] nil nil '(center repeated))
    (define-fringe-bitmap 'git-gutter-fr:deleted [128 192 224 240] nil nil 'bottom))
#+end_src   
** JDecomp
   #+begin_src emacs-lisp
     (use-package jdecomp
       :ensure t
       :config
     (customize-set-variable 'jdecomp-decompiler-paths
                             '((cfr . "~/.local/bin/cfr.jar"))
                             )
     )
   #+end_src
** ob-rasm2
#+begin_src emacs-lisp
  (use-package ob-rasm2
    :load-path "~/Git/ob-rasm2/"
    :config (add-to-list 'org-src-lang-modes '("rasm2" . asm)))
#+end_src
** ob-http
#+begin_src emacs-lisp
  (use-package ob-http
    :ensure t
    :config
    (org-babel-do-load-languages
     'org-babel-load-languages
     '((emacs-lisp . t)
       (http . t))))
#+end_src
* Org mode
** Initial Visibility
#+begin_src emacs-lisp
  (setq org-startup-folded t)
#+end_src
** Install org
#+begin_src emacs-lisp
  (use-package org
    :pin gnu)
#+end_src
** Org Structure Templates
   Working in org 9.2.4
   #+BEGIN_SRC emacs-lisp
     (add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
     (add-to-list 'org-structure-template-alist '("sh" . "src shell :results output"))
     (add-to-list 'org-structure-template-alist '("py" . "src python :results output"))
     (add-to-list 'org-structure-template-alist '("js" . "src js :results output"))
     (add-to-list 'org-structure-template-alist '("go" . "src go :results output"))
     (add-to-list 'org-structure-template-alist '("http" . "src http"))
     (require 'org-tempo)
   #+END_SRC
** Org Indent
   #+begin_src emacs-lisp
     (org-indent-mode 1)
     (add-hook 'org-mode-hook 'org-indent-mode)
   #+end_src
** Inline Images
   #+begin_src emacs-lisp
     (setq org-startup-with-inline-images t)
   #+end_src
** No number with lists
   #+BEGIN_SRC emacs-lisp
     (setq org-export-with-section-numbers nil)
   #+END_SRC
** Org bullets
   #+BEGIN_SRC emacs-lisp
     (use-package org-bullets
        :ensure t
        :init (add-hook 'org-mode-hook 'org-bullets-mode)
     )   
   #+END_SRC
** Org as a Word Processor
   #+BEGIN_SRC emacs-lisp
     (set-frame-font "Hasklig 13")
     (set-face-attribute 'default nil :height 100)
     (set-face-attribute 'default t :font "Hasklig 13")
     (add-to-list 'default-frame-alist '(font . "Hasklig 13"))

     (setq org-hide-emphasis-markers t) ;; Just the Italics

     (font-lock-add-keywords 'org-mode
                                 '(("^ +\\([-*]\\) "
                                    (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•")))))) ;; Better Bullets for lists
   #+END_SRC
*** Haskling-mode
    #+begin_src emacs-lisp
      (use-package hasklig-mode
        :ensure t
        :hook (haskell-mode)
        )
      (define-globalized-minor-mode my-global-haskling-mode hasklig-mode
        (lambda() (hasklig-mode 1)))
      (my-global-haskling-mode 1)
    #+end_src
** ox-reveal
   #+BEGIN_SRC emacs-lisp
     (use-package ox-reveal
       :ensure ox-reveal
       :config
       (setq org-reveal-root "http://cdn.jsdelivr.net/reveal.js/3.0.0/")
       (setq org-reveal-mathjax t)
     )

     (use-package htmlize
     :ensure t)
   #+END_SRC
** Hide leading stars
   #+BEGIN_SRC emacs-lisp
     (setq org-hide-leading-stars t)
   #+END_SRC
** Syntax Highlight code blocks
   #+BEGIN_SRC emacs-lisp
     (setq org-src-fontify-natively t)
   #+END_SRC
** Make TAB work as expected
   #+BEGIN_SRC emacs-lisp
     (setq org-src-tab-acts-natively t)
   #+END_SRC
** Don't include footer when exporting to html 
   #+BEGIN_SRC emacs-lisp
     (setq org-html-postamble nil)
   #+END_SRC
** Org babel
   #+BEGIN_SRC emacs-lisp
     (defadvice org-babel-execute-src-block (around load-language nil activate)
       "Load language if needed"
       (let ((language (org-element-property :language (org-element-at-point))))
         (unless (cdr (assoc (intern language) org-babel-load-languages))
           (add-to-list 'org-babel-load-languages (cons (intern language) t))
           (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages))
         ad-do-it))

     ;; dont ask just run the code block
     (setq org-confirm-babel-evaluate nil)

     ;; Set python3
     (setq org-babel-python-command "python3")
   #+END_SRC

install =ob-go= so I can execure golang within org-mode.
#+begin_src emacs-lisp
  (use-package ob-go
    :ensure t)
#+end_src
** Org Agenda 
   #+BEGIN_SRC emacs-lisp
     (setq org-agenda-files (list "~/Documents/org/index.org"
     "~/Documents/org/index.org"
     "~/Documents/org/birthdays.org"
     "~/Documents/org/anniversays.org"))
     (global-set-key (kbd "C-c a") 'org-agenda)
   #+END_SRC
** Tree Slide
   A quick way to display an org-mode file is using org-tree-slide.
   - org-tree-slide-move-next-tree (C->)
   - org-tree-slide-move-previous-tree (C-<)
   - org-tree-slide-content (C-x s c)

   #+BEGIN_SRC emacs-lisp
     (use-package org-tree-slide
        :ensure t
        :init
        (setq org-tree-slide-skip-outline-level 4)
        (org-tree-slide-simple-profile)
     )   
   #+END_SRC
** Twitter Bootstrap
   #+BEGIN_SRC emacs-lisp
     (use-package ox-twbs
       :ensure t)
   #+END_SRC
