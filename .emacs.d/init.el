(package-initialize)
(org-babel-load-file "~/.emacs.d/emacs.org")
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("7f6d4aebcc44c264a64e714c3d9d1e903284305fd7e319e7cb73345a9994f5ef" default))
 '(doom-modeline-buffer-encoding nil)
 '(doom-modeline-buffer-file-name-style 'buffer-name)
 '(doom-modeline-icon t)
 '(doom-modeline-major-mode-icon t)
 '(jdee-server-dir "~/opt/jdee-server/target")
 '(org-agenda-files '("~/Documents/org/index.org"))
 '(package-selected-packages
   '(vterm flycheck-swift swift-mode go-autocomplete ob-go ac-js2 nodejs-repl gitignore-mode gitignore-snippets use-package-hydra poetry ox-slack ox-ioslide exec-path-from-shell wttrin plain-org-wiki csharp-mode csv-mode bash-completion nord-theme powershell elixir-mode go-mode jdee ox-tufte ox-slimhtml ox-impress-js ox-html5slide ox-haunt ox-gfm shell-pop flyspell-correct-ivy pylint python-pylint exwm org org-bullets smartparens powerline use-package)))

(custom-set-faces
)
