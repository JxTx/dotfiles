[Appearance]
ColorScheme=nord
Font=Hasklig,10,-1,5,50,0,0,0,0,0,Regular

[Cursor Options]
CursorShape=1

[General]
Name=default
Parent=FALLBACK/
ShowTerminalSizeHint=false

[Interaction Options]
AutoCopySelectedText=true

[Keyboard]
KeyBindings=linux

[Scrolling]
HistoryMode=2
ScrollBarPosition=2

[Terminal Features]
BlinkingCursorEnabled=true
