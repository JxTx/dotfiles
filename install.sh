#!/bin/bash
# shellcheck disable=SC2039
# shellcheck disable=SC1117
# shellcheck disable=SC3010
# shellcheck disable=SC3020

# base raw url
REPOURL="https://gitlab.com/JxTx/dotfiles/-/raw/main/"

# tell me what you got
unamestr=$(uname)
if [[ "$unamestr" == "Linux" ]]
then
    OPERATING_SYSTEM="Debian"
elif [[ "$unamestr" == "Darwin" ]]
then
    OPERATING_SYSTEM="Darwin"
else
    echo "[!] unsupported or unknown OPERATING_SYSTEM"
    exit
fi

# deps
if [[ "$OPERATING_SYSTEM" == "Debian" ]]
then
    if ! command -c sudo &> /dev/null
    then
	    apt-get -qq install -y sudo
    fi
        sudo apt-get install -qq -y wget git
    
elif [[ "$OPERATING_SYSTEM" == "Darwin" ]]
then
    if ! type brew &> /dev/null
    then
	    echo "[!] brew not found.. go install brew"
	exit
    fi
    brew install wget
    brew install git
fi

# dir we will always need
mkdir -p "$HOME/.config/"

configure_emacs() {
    mkdir -p "$HOME/.emacs.d"
    wget "$REPOURL/.emacs.d/emacs.org" -O "$HOME/.emacs.d/emacs.org"
    wget "$REPOURL/.emacs.d/init.el" -O "$HOME/.emacs.d/init.el"
}

setup_shell() {
    wget "$REPOURL/.zshrc" -O "$HOME/.zshrc"
    wget "$REPOURL/.abbr_pwd" -O "$HOME/.abbr_pwd"
    wget "$REPOURL/.aliases" -O "$HOME/.aliases"
    wget "$REPOURL/.functions" -O "$HOME/.functions"
    wget "$REPOURL/.profile" -O "$HOME/.profile"
    wget "$REPOURL/.pylintrc" -O "$HOME/.pylintrc"
    wget "$REPOURL/.jshintrc" -O "$HOME/.jshintrc"
    wget "$REPOURL/.lldbinit" -O "$HOME/.lldbinit"

    mkdir -p "$HOME/.zfunc"
    wget "$REPOURL/.zfunc/_pipenv" -O "$HOME/.zfunc/_pipenv"
    wget "$REPOURL/.zfunc/_pipx" -O "$HOME/.zfunc/_pipx"
    wget "$REPOURL/.zfunc/_poetry" -O "$HOME/.zfunc/_poetry"
    wget "$REPOURL/grc.zsh" -O "$HOME/.grc.zsh"

    wget "$REPOURL/.tmux.conf" -O "$HOME/.tmux.conf"
    wget "$REPOURL/.procs.toml" -O "$HOME/.procs.toml"
    wget "$REPOURL/.htoprc" -O "$HOME/.htoprc"
    wget "$REPOURL/.radare2rc" -O "$HOME/.radare2rc"
    wget "$REPOURL/.ripgrep" -O "$HOME/.ripgrep"

    # specfic configs
    if [[ "$OPERATING_SYSTEM" == "Debian" ]]
    then
	    mkdir -p "$HOME/.config/openbox"
	    mkdir -p "$HOME/.config/tint2"

	    mkdir -p "$HOME/.local/share/konsole/"
	
	    wget "$REPOURL/debian/.config/konsolerc" -O "$HOME/.config/konsolerc"
	    wget "$REPOURL/debian/.config/openbox/autostart.sh" -O "$HOME/.config/openbox/autostart.sh"
	    wget "$REPOURL/debian/.config/openbox/menu.xml" -O "$HOME/.config/openbox/menu.xml"
        wget "$REPOURL/debian/.config/openbox/rc.xml" -O "$HOME/.config/openbox/rc.xml"
        wget "$REPOURL/debian/.config/tint2/tint2rc" -O "$HOME/.config/tint2/tint2rc"

        wget "$REPOURL/debian/.local/share/konsole/default.profile" -O "$HOME/.local/share/konsole/default.profile"
        wget "$REPOURL/debian/.local/share/konsole/nord.colorscheme" -O "$HOME/.local/share/konsole/nord.colorscheme"
        wget "$REPOURL/debian/.local/share/konsole/tabtheme.css" -O "$HOME/.local/share/konsole/tabtheme.css"

        wget "$REPOURL/debian/.gtkrc-2.0" -O "$HOME/.gtkrc-2.0"
        
    elif [[ "$OPERATING_SYSTEM" == "Darwin" ]]
    then
	    wget https://raw.githubusercontent.com/arcticicestudio/nord-iterm2/develop/src/xml/Nord.itermcolors \
	     -O "$HOME/.config/iterm2/Nord.itermcolors"

	mkdir -p "$HOME/Library/Developer/Xcode/UserData/FontAndColorThemes"
	    wget https://raw.githubusercontent.com/arcticicestudio/nord-xcode/develop/src/Nord.xccolortheme \
	     -O "$HOME/Library/Developer/Xcode/UserData/FontAndColorThemes/Nord.xccolortheme"
    fi	
}

pips_install() {
    if [[ "$OPERATING_SYSTEM" == "Debian" ]]
    then
	if ! command -c python3 &>/dev/null
	then
	    sudo apt-get -qq install -y python3 python3-pip python3-venv
	    python3 -m pip install --user pipx
	    python3 -m pipx ensurepath
	fi
	
    elif [[ "$OPERATING_SYSTEM" == "Darwin" ]]
    then
	if ! command -c pipx &>/dev/null
	then
	    brew install pipx
	    pipx ensurepath
	fi
    fi
    bash <(curl -fsSL "$REPOURL/pips")
}

npm_install() {
    # TODO some sort of check for npm
    bash <(curl -fsSL "$REPOURL/npms")
}

packages_install() {
    if [[ "$OPERATING_SYSTEM" == "Debian" ]]
    then
	    bash <(curl -fsSL "$REPOURL/debian/apt")

	# fonts
	    VERSION=1.2
	    DIR=/usr/share/fonts
	    FILE="Hasklig-${VERSION}.zip"
	    sudo wget "https://github.com/i-tu/Hasklig/releases/download/v${VERSION}/Hasklig-${VERSION}.zip"  \
	     -O $DIR/$FILE
	    cd $DIR && \
	        sudo unzip $FILE
	
    elif [[ "$OPERATING_SYSTEM" == "Darwin" ]]
    then
	    brew install rcmdnk/file/brew-file
	    mkdir -p "$HOME/.config/brewfile/"
	    wget "$REPOURL/Darwin/Brewfile" -O "$HOME/.config/brewfile/Brewfile"
	    brew file install
	    #"$(brew --prefix)/opt/fzf/install"
    fi
}

get_thirdparty() {
    if [ ! -d "$HOME/opt/" ]
    then
	mkdir -p "$HOME/opt/"
    fi

    git clone https://github.com/danielmiessler/SecLists "$HOME/opt/SecLists"
    git clone https://github.com/DerekSelander/LLDB "$HOME/opt/LLDB"

}

if [[ -z "$1" ]]
then
       packages_install
       pips_install
       npm_install
       configure_emacs
       setup_shell
       get_thirdparty
elif [[ "$1" == "--config" ]]
then
       configure_emacs
       setup_shell
else
    echo -e "[!] provide no arguments, or --config for just the dotfiles"
fi
