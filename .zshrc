setopt appendhistory
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '$HOME/.zshrc'
fpath+=~/.zfunc

if type brew &>/dev/null; then
  FPATH=$(brew --prefix)/share/zsh/site-functions:$FPATH
  FPATH=$(brew --prefix)/share/zsh-completions:$FPATH # % compaudit | xargs chmod g-w
  
  autoload -Uz compinit
  compinit
fi

autoload -U bashcompinit
bashcompinit
autoload -U promptinit
promptinit

OPERATING_SYSTEM=$(uname)
if [[ "$OPERATING_SYSTEM" == 'Linux' ]]
then
    export PATH=/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:$HOME/.local/bin:$HOME/go/bin:$HOME/go/bin
elif [[ "$OPERATING_SYSTEM" == 'Darwin' ]]
then
    autoload -Uz compinit && compinit -C
    #source <(ipsw completion zsh)
    #compdef _ipsw ipsw

    export PATH=/opt/homebrew/bin:/opt/homebrew/sbin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/Library/TeX/texbin:/Library/Apple/usr/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:$HOME/.local/bin:$HOME/go/bin:$HOME/go/bin:/usr/local/Cellar/python@3.9/3.9.2_1/Frameworks/Python.framework/Versions/3.9/bin:/usr/local/opt/binutils/bin:/Library/Developer/CommandLineTools/Library/Frameworks/Python3.framework/Versions/3.9/bin

    source $(brew --prefix)/share/zsh/site-functions
    source $(brew --prefix)/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
    source $(brew --prefix)/share/zsh-autosuggestions/zsh-autosuggestions.zsh

    # export NVM_DIR="$HOME/.nvm"
    # [ -s "/opt/homebrew/opt/nvm/nvm.sh" ] && . "/opt/homebrew/opt/nvm/nvm.sh"  # This loads nvm
    # [ -s "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm" ] && . "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm"  # This loads nvm bash_completion
    
    # set jtool2 colour output
    export JCOLOR=1
    
    XCRUN=$(which xcrun)
    if [ ! -z $XCRUN ];
    then
	    export macosx_sdk="$($XCRUN --show-sdk-path -sdk macosx)/"
        #export macos_sdk="$macosx_sdk"
        export iphoneos_sdk="$($XCRUN --show-sdk-path -sdk iphoneos)/"
        #export ios_sdk="$iphoneos_sdk"
    fi

    export kdks="/Library/Developer/KDKs/"
    
fi

# the prompt
git_prompt() {
 ref=$(git symbolic-ref HEAD | cut -d'/' -f3)
 echo $ref
}

source ~/.abbr_pwd
precmd() {
    if [ "$TERM_PROGRAM" = "iTerm.app" ]
    then
       # Needed because of this: https://gitlab.com/gnachman/iterm2/-/issues/8755
       window_title="\e]0;$(felix_pwd_abbr)\a"
       echo -ne "$window_title"
       source $HOME/.iterm2_shell_integration.zsh
    fi

    if [ -d ".git" ] && [ $VIRTUAL_ENV ]
    then
        export PS1="%F{8}[%F{4}$(felix_pwd_abbr)%F{8} %f$(basename $VIRTUAL_ENV) %f$(git_prompt)%F{8}]%% %f"
    elif [ $VIRTUAL_ENV ]
    then
	    export PS1="%F{8}[%F{4}$(felix_pwd_abbr)%F{8} %f$(basename $VIRTUAL_ENV)%F{8}]%% %f"
    elif [ -d ".git" ]
    then
        export PS1="%F{8}[%F{4}$(felix_pwd_abbr)%F{8} %f$(git_prompt)%F{8}]%% %f"
    else
	    export PS1="%F{8}[%F{4}$(felix_pwd_abbr)%F{8}]%% %f"
    fi
}

#export PS1="[%F{yellow}%~%f]%# "

#source ~/.dockerfunc
source ~/.functions
source ~/.aliases

# golang
export GOPATH=$HOME/go/

# for grc command colours
[[ -s ~/.grc.zsh ]] && source ~/.grc.zsh

# ripgrep settings
export RIPGREP_CONFIG_PATH=~/.ripgrep

# use bat for less
export MANPAGER="sh -c 'col -bx | bat --theme Nord -l man -p'"

# for pipx
eval "$(register-python-argcomplete pipx)"


[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh"

